package com.psgs.mythsorigins.controller;

import com.psgs.mythsorigins.entity.Origins;
import com.psgs.mythsorigins.service.IOriginsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/origins")
public class OriginsController {

    @Autowired
    IOriginsService originsService;

    @GetMapping
    public List<Origins> getAllOrigins () {
        return originsService.getAll();
    }

    @GetMapping("/{id_origins}")
    public Origins getOne(@PathVariable(value = "id_origins") int id_origins) {
        return originsService.get(id_origins);
    }

    @PostMapping
    public void add (@RequestBody Origins origins) {
        originsService.post(origins);
    }

    @PutMapping("/{id_origins}")
    public void update (@RequestBody Origins origins, @PathVariable(value = "id_origins") int id_origins) {
        originsService.put(origins, id_origins);
    }

    @DeleteMapping("/{id_origins}")
    public void delete(Origins origins, @PathVariable(value = "id_origins") int id_origins) {
        originsService.delete(id_origins);
    }

}
