package com.psgs.mythsorigins.service;

import com.psgs.mythsorigins.dao.IOriginsDao;
import com.psgs.mythsorigins.entity.Origins;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OriginsServiceImpl implements IOriginsService {

    @Autowired
    IOriginsDao originsDao;

    @Override
    public Origins get(Integer id_origins) {
        return originsDao.findById(id_origins).get();
    }

    @Override
    public List<Origins> getAll() {
        return (List<Origins>) originsDao.findAll();
    }

    @Override
    public void post(Origins origins) {
        originsDao.save(origins);
    }

    @Override
    public void put(Origins origins, int id_origins) {
        originsDao.findById(id_origins).ifPresent((x)->{
            origins.setId_origins(id_origins);
            originsDao.save(origins);
        });
    }

    @Override
    public void delete(int id_origins) {
        originsDao.deleteById(id_origins);
    }

}
