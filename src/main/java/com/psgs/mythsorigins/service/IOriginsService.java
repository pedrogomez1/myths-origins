package com.psgs.mythsorigins.service;

import com.psgs.mythsorigins.entity.Origins;

import java.util.List;

public interface IOriginsService {

    public Origins get (Integer id);
    public List<Origins> getAll();
    public void post(Origins origins);
    public void put(Origins origins, int id);
    public void delete(int id);

}
