package com.psgs.mythsorigins.dao;

import com.psgs.mythsorigins.entity.Origins;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IOriginsDao extends CrudRepository<Origins, Integer> {

}
