package com.psgs.mythsorigins.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "origins")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Origins {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_origins;
    private String origin;

}
